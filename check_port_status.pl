#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor port-state                                  #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-08-11                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use Net::SNMP;
use File::Basename;

#        Global OID-Sets
#############################
my %port = (
				name       => "1.3.6.1.2.1.2.2.1.2",
				adminState => "1.3.6.1.2.1.2.2.1.7",
				operState  => "1.3.6.1.2.1.2.2.1.8",
);

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my $device;
my $host;
my $community;
my $version;
my $session;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);

#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Port-status check-script Version 2016-08-11 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check port-status\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -H <hostaddress> -C <SNMP-community> (-v [1|2c]) -p <Port>\n";
	print "\n";
	print "Options:\n";
	print "    -H       hostname or address\n";
	print "    -C       SNMP-read-community-name (default: public)\n";
	print "    -v       SNMP-version (default: 1)\n";
	print "    -p       Port-name\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$host = shift(@ARGV);
		}
		
		# Decode community
		elsif ("$temp" eq "-C") {
			$community = shift(@ARGV);
		}
		
		# Decode version
		elsif ("$temp" eq "-v") {
			$version = shift(@ARGV);
		}
		
		# Decode port-name
		elsif ("$temp" eq "-p") {
			$device = shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$host) {
		usage();
	}
	if (!$device) {
		usage();
	}
		if (!$community) {
		$community = "public";
	}
	if ("$version" ne "2c") {
		$version = 1;
	}
}

# SNMP-initialization
sub snmpSetup{
	my ($snmpSession, $snmpError) = Net::SNMP->session(
														-hostname  => $host,
														-version   => $version,
														-community => $community);
	if ($snmpError) {
		print "SNMP encountered an error: $snmpError\n";
		exit $exitStatus{critical};
	}
	
	return $snmpSession;
}

# Check Returns
sub checkReturn{
	my ($value, $name) = @_;
	if (!$value){
		print "Device returned no result for $name.\n";
		exit $exitStatus{unknown};
	}
}

# Check port-status
sub checkPortStatus{
	my $string;
	my $result = $exitStatus{unknown};
	
	my $rName = $session->get_table($port{name});
	foreach my $oid (keys %$rName){
		my $name = "$$rName{$oid}";
		checkReturn($name,"port-name\n");
		my $id = $oid;
		$id =~ s/$port{name}\.//;
		
		# If name of port matches requested port
		if("$name" eq "$device"){
			my $rOS = $session->get_request(-varbindlist=>["$port{operState}.$id"]);
			my $OS = $rOS->{"$port{operState}.$id"};
			checkReturn($OS,"OperState for $name");
			
			my $rAS = $session->get_request(-varbindlist=>["$port{adminState}.$id"]);
			my $AS = $rAS->{"$port{adminState}.$id"};
			checkReturn($AS,"AdminState for $name");
			
			if (($AS == 1) && ($OS == 2)){
				$result = $exitStatus{critical};
				$string = "Status CRITCAL: $name DOWN\n";
			} elsif (($AS == 1) && ($OS == 1)){
				$result = $exitStatus{ok};
				$string = "Status OK: $name UP\n";
			} elsif (($AS == 2) && ($OS == 2)){
				$result = $exitStatus{ok};
				$string = "Status OK: $name DOWN\n";
			} else {
				$result = $result = $exitStatus{unknown};
				$string = "Status UNKNONW: $name\n";
			}
		}
	}
	
	print $string;
	exit $result;
}

#        Main
#############################
decodeArguments();
$session = snmpSetup($host, $version, $community);
checkPortStatus();