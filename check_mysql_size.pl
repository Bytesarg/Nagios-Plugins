#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor mysql-db-size                               #
#        By bytesarg (c) 2017                                                #
#        Version: 2017-04-19                                                 #
##############################################################################

#        Use-Imports
#############################
use strict;
use File::Basename;
use DBI;

#        Global variables
#############################

my $path = dirname($0);
my $scriptName = basename($0);
my $warning;
my $critical;
my $hostname = "127.0.0.1";
my $user = "root";
my $password;

#        status-codes
#############################

my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);

#        SubFunctons
#############################

# Usage-help
sub usage{
	print "mySQL-DB-Size check-script Version 2017-04-19 (c) by bytesarg\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName [-H <hostaddress>] [-u <user>] -p <password> [-w <warning>] [-c <critical>]\n";
	print "\n";
	print "Options:\n";
	print "    -H       Hostname or address (default: 127.0.0.1)\n";
	print "    -u       Username for mySQL login (default: root)\n";
	print "    -p       Password for mySQL login\n";
	print "    -w       warning-value in MB (default: 150MB)\n";
	print "    -c       critical-value in MB (default: 200MB)\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$hostname = shift(@ARGV);
		}

		# Decode password
		elsif ("$temp" eq "-p") {
			$password = shift(@ARGV);
		}
		
		# Decode user
		elsif ("$temp" eq "-u") {
			$user = shift(@ARGV);
		}

		# Decode warning values
		elsif ("$temp" eq "-w") {
			$warning = shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			$critical = shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	if (!$warning){
		$warning = 150;
	}
	if (!$critical){
		$critical = 200;
	}
}

# Check DB-Size
sub main{
	my $dbh = DBI->connect("DBI:mysql:;host=$hostname",$user,"$password") || die "Could not connect to database: $DBI::errstr";
	my $statement = $dbh->prepare('SELECT table_schema "Data Base Name", ((data_length + index_length) / 1024 / 1024) as "Size in MB" FROM information_schema.TABLES');
	my $results = $dbh->selectall_hashref('SELECT table_schema "Data Base Name", ((data_length + index_length) / 1024 / 1024) as "Size in MB" FROM information_schema.TABLES','Data Base Name');

	my $readableC;
	my $readableW;
	my $perfdata;

	my $criticalCount=0;
	my $warningCount=0;
	my $totalCount=0;

	foreach my $name (keys %$results){
		my $value = $results->{$name}->{'Size in MB'};
		if ($value > $critical){
			$criticalCount ++;
			$readableC = "$readableC $name";
		}elsif ($value > $warning){
			$warningCount ++;
			$readableW = "$readableW $name";
		}
		$totalCount ++;
		$perfdata = "$perfdata $name=$value;$warning;$critical";
	}
	$dbh->disconnect();

	if($criticalCount>0){
		print "CRITICAL: $readableC | $perfdata\n";
		exit $exitStatus{critical};
	}elsif($warningCount>0){
		print "WARNING: $readableW | $perfdata\n";
		exit $exitStatus{warning};
	}else{
		print "OK: $totalCount DBs within parameters | $perfdata\n";
		exit $exitStatus{ok};
	}
	
}

#        Main
#############################
decodeArguments();
main();
