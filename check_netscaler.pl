#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor citrix-netscaler                            #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-07-28                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use Net::SNMP;
use File::Basename;

#        Global OID-Sets
#############################
my %device = (
	cpu => {
		name  => "1.3.6.1.4.1.5951.4.1.1.41.6.1.1",
		value => "1.3.6.1.4.1.5951.4.1.1.41.6.1.2",
	},
	health => {
		name  => "1.3.6.1.4.1.5951.4.1.1.41.7.1.1",
		value => "1.3.6.1.4.1.5951.4.1.1.41.7.1.2",
	},
	memory => {
		used  => "1.3.6.1.4.1.5951.4.1.1.41.2.0",
		total => "1.3.6.1.4.1.5951.4.1.1.41.4.0",
	},
);

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my @units = ("MB","GB","TB");
my $host;
my $community;
my $version;
my $session;
my $function;
my @warning;
my @critical;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);


#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Citrix-Netscaler check-script Version 2016-07-28 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check Citrix Netscaler-devices\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -H <hostaddress> -C <SNMP-community> (-v [1|2c])(-t [cpu|mem|temp|fan|power]) -w <value> -c <value>\n";
	print "\n";
	print "Options:\n";
	print "    -H       hostname or address\n";
	print "    -C       SNMP-read-community-name (default: public)\n";
	print "    -v       SNMP-version (default: 1)\n";
	print "    -t       check-type one of:\n";
	print "             cpu:   check cpu-load (default) (warning and critical default to 50% & 75%\n";
	print "             mem:   check memory-load\n";
	print "             temp:  check temperature-sensors (warning and critical may be reported by device / default to 50 & 60)\n";
	print "             fan:   check fan status\n";
	print "             power: check power-supply status\n";
	print "    -w       warning-value, only one max or min:max value supported\n";
	print "    -c       critical-value, only one max or min:max value supported\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$host = shift(@ARGV);
		}
		
		# Decode community
		elsif ("$temp" eq "-C") {
			$community = shift(@ARGV);
		}
		
		# Decode version
		elsif ("$temp" eq "-v") {
			$version = shift(@ARGV);
		}
		
		# Decode type
		elsif ("$temp" eq "-t") {
			$function = shift(@ARGV);
		}
		
		# Decode warning values
		elsif ("$temp" eq "-w") {
			@warning = split ":",shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			@critical = split ":",shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$host) {
		usage();
	}
	if (!$community) {
		$community = "public";
	}
	if ("$version" ne "2c") {
		$version = 1;
	}

	if (!$function) {
		$function = "cpu";
	}
	if (!$warning[0]){
		$warning[0] = 0;
	}
	if (!$critical[0]){
		$critical[0] = 0;
	}
}

# SNMP-initialization
sub snmpSetup{
	my ($snmpSession, $snmpError) = Net::SNMP->session(
														-hostname  => $host,
														-version   => $version,
														-community => $community);
	if ($snmpError) {
		print "SNMP encountered an error: $snmpError\n";
		exit $exitStatus{critical};
	}
	
	return $snmpSession;
}

# Check Returns
sub checkReturn{
	my ($value, $name) = @_;
	if (!$value){
		print "Device returned no result for $name.\n";
		exit $exitStatus{unknown};
	}
}

# Check CPU
sub checkCpu {
	my $rCPU = $session->get_table($device{cpu}{value});
	my $warnings = 0;
	my $criticals = 0;
	my $string;
	my $perfdata;
	
	# Parse cpu-load
	foreach my $oid (keys %$rCPU){
		my $load = "$$rCPU{$oid}";
		my $id = $oid;
		$id =~ s/$device{cpu}{value}\.//;
		my $rName = $session->get_request(-varbindlist=> ["$device{cpu}{name}.$id"]);
		my $name = $rName->{"$device{cpu}{name}.$id"};
		
		# Defaults
		if (!$critical[0]){
			$critical[0] = 75;
		}
		if (!$warning[0]){
			$warning[0] = 50;
		}
		
		if ($load > $critical[0]){
			$criticals++;
		} elsif ($load > $warning[0]){
			$warnings++;
		}
		# Generate Strings
		$string = "$string $name: $load%";
		
		$name =~ s/\ /_/g;
		$perfdata = "$perfdata $name=$load";
	}
	
	# Generate output
	if ($criticals > 0){
		print "CPU CRITICAL:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "CPU WARNING:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "CPU OK:$string |$perfdata\n";
		exit $exitStatus{ok};
	}
}

# Check Memory
sub checkMem{
	my $rUsed  = $session->get_request(-varbindlist=> ["$device{memory}{used}"]);
	my $rTotal = $session->get_request(-varbindlist=> ["$device{memory}{total}"]);
	my $used  = $rUsed ->{"$device{memory}{used}"};
	my $total = $rTotal->{"$device{memory}{total}"};
	my $load = $total * $used / 100;
	
	# Make values human-readable
	my $memTotalH = $total;
	my $memUsedH = $load;
	my $unit = 0;
	while ($memTotalH > 4096){
		$memTotalH = $memTotalH / 1024;
		$unit++;
	}
	my $i=0;
	while ($i < $unit){
		$memUsedH = $memUsedH / 1024;
		$i++;
	}
	
	# Round numbers
	$memTotalH = sprintf "%.2f", $memTotalH;
	$memUsedH = sprintf "%.2f", $memUsedH;
	$memTotalH = "$memTotalH".$units[$unit];
	$memUsedH = "$memUsedH".$units[$unit];
	
	if ($used > $critical[0]){
		print "Memory CRITICAL: $memUsedH of $memTotalH used ($used\%) | memory_total=$total memory_used=$load\n";
		exit $exitStatus{critical};
	} elsif ($used > $warning[0]){
		print "Memory WARNING: $memUsedH of $memTotalH used ($used\%) | memory_total=$total memory_used=$load\n";
		exit $exitStatus{warning};
	} else {
		print "Memory OK: $memUsedH of $memTotalH used ($used\%) | memory_total=$total memory_used=$load\n";
		exit $exitStatus{ok};
	}
}

# Check health by filter
sub checkHealth{
	my $filter = shift(@_);
	
	my $rName = $session->get_table($device{health}{name});
	my $warnings = 0;
	my $criticals = 0;
	my $string;
	my $perfdata;
	
	# Get Values
	foreach my $oid (keys %$rName){
		my $name = "$$rName{$oid}";
		if (index($name,$filter) >= 0){
			my $id = $oid;
			$id =~ s/$device{health}{name}\.//;
			my $rValue = $session->get_request(-varbindlist=> ["$device{health}{value}.$id"]);
			my $value = $rValue->{"$device{health}{value}.$id"};
			
			# Virtual machines and not supplied values default to 0
			if ($value > 0){
			# If only maxima are supplied
				if (!$critical[1]){
					if ($value > $critical[0]){
						$criticals++;
					} elsif ($value > $warning[0]){
						$warnings++;
					}
			
			# else if maxima and minima are supplied
				} else {
					if (($value > $critical[1]) || ($value < $critical[0])){
						$criticals++;
					} elsif (($value > $warning[1]) || ($value < $warning[0])){
						$warnings++;
					}
				}
			
				$string = "$string $name: $value";
			
				$name =~ s/\ /_/g;
				$perfdata = "$perfdata $name=$value";
			}
		}
	}
	
	# Generate output
	if ($criticals > 0){
		print "$filter CRITICAL:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "$filter WARNING:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "$filter OK:$string |$perfdata\n";
		exit $exitStatus{ok};
	}
}

#        Main
#############################
decodeArguments();
$session = snmpSetup($host, $version, $community);

if ($function eq "mem"){
	checkMem();
} elsif ($function eq "cpu"){
	checkCpu();
} elsif ($function eq "temp"){
	checkHealth("Temperature");
} elsif ($function eq "fan"){
	checkHealth("Fan");
} elsif ($function eq "power"){
	checkHealth("Power");
} else {
	usage();
}