#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor toner-state                                 #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-08-02                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use Net::SNMP;
use File::Basename;

#        Global OID-Sets
#############################
my %toner = (
				name     => "1.3.6.1.2.1.43.11.1.1.6",
				capacity => "1.3.6.1.2.1.43.11.1.1.8",
				level    => "1.3.6.1.2.1.43.11.1.1.9",
);

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my $host;
my $community;
my $version;
my $session;
my $warning;
my $critical;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);

#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Toner check-script Version 2016-08-02 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check Toner of Printer-MIB compatible devices\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -H <hostaddress> -C <SNMP-community> (-v [1|2c]) -w <value> -c <value>\n";
	print "\n";
	print "Options:\n";
	print "    -H       hostname or address\n";
	print "    -C       SNMP-read-community-name (default: public)\n";
	print "    -v       SNMP-version (default: 1)\n";
	print "    -w       warning-value\n";
	print "    -c       critical-value\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$host = shift(@ARGV);
		}
		
		# Decode community
		elsif ("$temp" eq "-C") {
			$community = shift(@ARGV);
		}
		
		# Decode version
		elsif ("$temp" eq "-v") {
			$version = shift(@ARGV);
		}
		
		# Decode warning values
		elsif ("$temp" eq "-w") {
			$warning = shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			$critical = shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$host) {
		usage();
	}
	if (!$community) {
		$community = "public";
	}
	if ("$version" ne "2c") {
		$version = 1;
	}

	if (!$warning){
		$warning = 25;
	}
	if (!$critical){
		$critical = 5;
	}
}

# SNMP-initialization
sub snmpSetup{
	my ($snmpSession, $snmpError) = Net::SNMP->session(
														-hostname  => $host,
														-version   => $version,
														-community => $community);
	if ($snmpError) {
		print "SNMP encountered an error: $snmpError\n";
		exit $exitStatus{critical};
	}
	
	return $snmpSession;
}

# Check Returns
sub checkReturn{
	my ($value, $name) = @_;
	if (!$value){
		print "Device returned no result for $name.\n";
		exit $exitStatus{unknown};
	}
}

# Check Toner
sub checkToner{
	my $criticals = 0;
	my $warnings = 0;
	my $string;
	my $sum = 0;
	my $perfdata;
	
	my $rName = $session->get_table($toner{name});
	foreach my $oid (keys %$rName){
		my $name = "$$rName{$oid}";
		checkReturn($name,"device-name\n");
		my $id = $oid;
		$id =~ s/$toner{name}\.//;
		
		# Get values
		my $rCapacity = $session->get_request(-varbindlist=>["$toner{capacity}.$id"]);
		my $rLevel = $session->get_request(-varbindlist=>["$toner{level}.$id"]);
		my $capacity = $rCapacity->{"$toner{capacity}.$id"};
		checkReturn($capacity,"$name capacity\n");
		my $level = $rLevel->{"$toner{level}.$id"};
		checkReturn($level,"$name level\n");
		
		# Calculate percentage
		my $percent = $level * 100 / $capacity;
		$percent = sprintf "%.1f",$percent;
		
		# Generate return-string
		if ($percent < $critical){
			$criticals++;
			$string = "$string $name: $percent%";
		} elsif ($percent < $warning){
			$warnings++;
			$string = "$string $name: $percent%";
		}
		$sum++;
		$name =~ s/\ /_/g;
		$name =~ s/,//g;
		$perfdata = "$perfdata $name=$percent"
	}
	
	# Generate Output
	my $ok = $sum - $warnings - $criticals;
	if($criticals){
		print "Toner CRITICAL: $ok of $sum OK, failed:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings){
		print "Toner WARNING: $ok of $sum OK, failed:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "Toner OK: $ok of $sum OK |$perfdata\n";
		exit $exitStatus{ok};
	}
}

#        Main
#############################
decodeArguments();
$session = snmpSetup($host, $version, $community);
checkToner();
