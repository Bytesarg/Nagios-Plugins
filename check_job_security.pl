#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor job-security                                #
#        By Bytesarg (c) 2016                                                #
#        Version: 2016-07-28                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use File::Basename;

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my $warning = 50;
my $critical = 25;
my $user;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);

#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Job-security-check-script Version 2016-07-28 (c) by Bytesarg\n";
	print "\n";
	print "Use magic to check for the supplied users job-security\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -u <UserName> -w <value> -c <value>\n";
	print "\n";
	print "Options:\n";
	print "    -u       user-name\n";
	print "    -w       warning-value\n";
	print "    -c       critical-value\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode user
		if ("$temp" eq "-u") {
			$user = shift(@ARGV);
		}
		
		# Decode warning values
		elsif ("$temp" eq "-w") {
			$warning = shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			$critical = shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$user) {
		usage();
	}
}

# Check Job Security
sub checkJobSec{
	my $sec=0;
	if($user eq "bytesarg"){
		$sec = 60 + rand(40);
	} elsif ($user eq "flatline"){
		$sec = 60 + rand(40);
	} elsif ($user eq "compilenix"){
		$sec = 60 + rand(40);
	} else {
		$sec = rand(100);
	}
	
	$sec = sprintf "%.0f",$sec;
	
	if ($sec < $critical){
		print "Job-Security CRITICAL for user $user($sec%) | $user=$sec\n";
	} elsif ($sec < $warning){
		print "Job-Security Warning for user $user($sec%) | $user=$sec\n";
	} else {
		print "Job-Security OK for user $user($sec%) | $user=$sec\n";
	}
}

#        Main
#############################
decodeArguments();
checkJobSec();