#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor cisco-devices                               #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-08-05                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use Net::SNMP;
use File::Basename;

#        Global OID-Sets
#############################
# Catalyst
my %switch = (
		catalyst => {
			cpu => {
					value5s => "1.3.6.1.4.1.9.2.1.56.0",
					value1m => "1.3.6.1.4.1.9.2.1.57.0",
					value5m => "1.3.6.1.4.1.9.2.1.58.0",
			},
			mem => {
					used => "1.3.6.1.4.1.9.9.48.1.1.1.5.1",
					free => "1.3.6.1.4.1.9.9.48.1.1.1.6.1",
			},
			temp => {
					value => "1.3.6.1.4.1.9.9.13.1.3.1.3",
					name  => "1.3.6.1.4.1.9.9.13.1.3.1.2",
					limit => "1.3.6.1.4.1.9.9.13.1.3.1.4",
			},
			fan => {
					status  => "1.3.6.1.4.1.9.9.13.1.4.1.3",
					name    => "1.3.6.1.4.1.9.9.13.1.4.1.2",
					normal  => "1",
					warning => "2",
			},
			power => {
					status => "1.3.6.1.4.1.9.9.13.1.5.1.3",
					name   => "1.3.6.1.2.1.47.1.1.1.1.7",
					normal => "1",
					filter => ".",
			},
		},

# Nexus
		nexus => {
			cpu => {
					value5s => "1.3.6.1.4.1.9.9.305.1.1.1.0",
					value1m => "x",
					value5m => "x",
			},
			mem => {
					used => "1.3.6.1.4.1.9.9.109.1.1.1.1.12.1",
					free => "1.3.6.1.4.1.9.9.109.1.1.1.1.13.1",
			},
			temp => {
					value  => "1.3.6.1.4.1.9.9.91.1.1.1.1.4",
					name   => "1.3.6.1.2.1.47.1.1.1.1.7",
					limit => "1.3.6.1.4.1.9.9.91.1.1.1.1.7",
			},
			fan => {
					status  => "1.3.6.1.4.1.9.9.117.1.4.1.1.1",
					name    => "1.3.6.1.2.1.47.1.1.1.1.7",
					normal  => "2",
					warning => "x",
			},
			power => {
					status => "1.3.6.1.4.1.9.9.117.1.1.2.1.2",
					name   => "1.3.6.1.2.1.47.1.1.1.1.7",
					normal => "2",
					filter => "47", # Powersupplies contain "47"
			},
		},
		
# MDS
		mds => {
			cpu => {
					value5s => "1.3.6.1.4.1.9.9.305.1.1.1.0",
					value1m => "x",
					value5m => "x",
			},
			mem => {
					used => "1.3.6.1.4.1.9.9.305.1.1.2.0",
					free => "x",
			},
			temp => {
					value  => "1.3.6.1.4.1.9.9.91.1.1.1.1.4",
					name   => "1.3.6.1.2.1.47.1.1.1.1.7",
					limit => "1.3.6.1.4.1.9.9.91.1.2.1.1.4",
			},
			fan => {
					status  => "1.3.6.1.4.1.9.9.117.1.4.1.1.1",
					name    => "1.3.6.1.2.1.47.1.1.1.1.7",
					normal  => "2",
					warning => "x",
			},
			power => {
					status => "1.3.6.1.4.1.9.9.117.1.1.2.1.2",
					name   => "1.3.6.1.2.1.47.1.1.1.1.7",
					normal => "2",
					filter => "47", # Powersupplies contain "47"
			},
		}
	);

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my @units = ("B","KB","MB","GB","TB");
my $host;
my $community;
my $version;
my $session;
my $function;
my $device;
my @warning;
my @critical;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);


#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Cisco check-script Version 2016-08-05 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check Cisco devices\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -H <hostaddress> -C <SNMP-community> (-v [1|2c])(-t [cpu|mem|temp|fan|power]) (-d [catalyst|nexus|mds]) -w <value> -c <value>\n";
	print "\n";
	print "Options:\n";
	print "    -H       hostname or address\n";
	print "    -C       SNMP-read-community-name (default: public)\n";
	print "    -v       SNMP-version (default: 1)\n";
	print "    -t       check-type one of:\n";
	print "             cpu:   check cpu-load (default) (warning and critical default to 50% & 75%\n";
	print "             mem:   check memory-load\n";
	print "             temp:  check temperature-sensors (warning and critical may be reported by device / default to 50 & 60)\n";
	print "             fan:   check fan status\n";
	print "             power: check power-supply status\n";
	print "    -d       catalyst (default), nexus or mds device\n";
	print "    -w       warning-value\n";
	print "    -c       critical-value\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$host = shift(@ARGV);
		}
		
		# Decode community
		elsif ("$temp" eq "-C") {
			$community = shift(@ARGV);
		}
		
		# Decode version
		elsif ("$temp" eq "-v") {
			$version = shift(@ARGV);
		}
		
		# Decode type
		elsif ("$temp" eq "-t") {
			$function = shift(@ARGV);
		}
		
		# Decode device
		elsif ("$temp" eq "-d") {
			$device = shift(@ARGV);
		}
		
		# Decode warning values
		elsif ("$temp" eq "-w") {
			@warning = split ",",shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			@critical = split ",",shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$host) {
		usage();
	}
	if (!$community) {
		$community = "public";
	}
	if ("$version" ne "2c") {
		$version = 1;
	}
	if (("$device" ne "nexus") && ("$device" ne "mds")) {
		$device = "catalyst";
	}
	if (!$function) {
		$function = "cpu";
	}
	if (!$warning[0]){
		$warning[0] = 0;
	}
	if (!$critical[0]){
		$critical[0] = 0;
	}
}

# SNMP-initialization
sub snmpSetup{
	my ($snmpSession, $snmpError) = Net::SNMP->session(
														-hostname  => $host,
														-version   => $version,
														-community => $community);
	if ($snmpError) {
		print "SNMP encountered an error: $snmpError\n";
		exit $exitStatus{critical};
	}
	
	return $snmpSession;
}

# Check Returns
sub checkReturn{
	my ($value, $name) = @_;
	if (!$value){
		print "Device returned no result for $name.\n";
		exit $exitStatus{unknown};
	}
}

# Check memory
sub checkMemory{
	# Get values
	# Used Memory
	my $rMemUsed = $session->get_request(-varbindlist=> [$switch{$device}{mem}{used}]);
	my $memUsed = $rMemUsed->{$switch{$device}{mem}{used}};
	checkReturn($memUsed, "used memory");
	# Free Memory
	my $rMemFree = $session->get_request(-varbindlist=> [$switch{$device}{mem}{free}]);
	my $memFree = $rMemFree->{$switch{$device}{mem}{free}};
	checkReturn($memFree, "free memory");
	# Total Memory
	my $memTotal = $memFree + $memUsed;
	
	# Make values human-readable
	my $memTotalH = $memTotal;
	my $memUsedH = $memUsed;
	my $unit = 0;
	while ($memTotalH > 4096){
		$memTotalH = $memTotalH / 1024;
		$unit++;
	}
	my $i=0;
	while ($i < $unit){
		$memUsedH = $memUsedH / 1024;
		$i++;
	}
	
	# Round numbers
	$memTotalH = sprintf "%.2f", $memTotalH;
	$memUsedH = sprintf "%.2f", $memUsedH;
	$memTotalH = "$memTotalH".$units[$unit];
	$memUsedH = "$memUsedH".$units[$unit]; 
	
	# Calculate Percentage
	my $memPercent = sprintf "%.0f", ($memUsed * 100 / $memTotal);
	
	if ($memPercent > $critical[0]){
		print "Memory CRITICAL: $memUsedH of $memTotalH used ($memPercent\%) | memory_total=$memTotal memory_used=$memUsed\n";
		exit $exitStatus{critical};
	} elsif ($memPercent > $warning[0]){
		print "Memory WARNING: $memUsedH of $memTotalH used ($memPercent\%) | memory_total=$memTotal memory_used=$memUsed\n";
		exit $exitStatus{warning};
	} else {
		print "Memory OK: $memUsedH of $memTotalH used ($memPercent\%) | memory_total=$memTotal memory_used=$memUsed\n";
		exit $exitStatus{ok};
	}
}

# Check memory
sub checkMemoryMds{
	# Get values
	# Used Memory
	my $rMemUsed = $session->get_request(-varbindlist=> [$switch{$device}{mem}{used}]);
	my $memUsed = $rMemUsed->{$switch{$device}{mem}{used}};
	checkReturn($memUsed, "used memory");
		
	
	if ($memUsed > $critical[0]){
		print "Memory CRITICAL: $memUsed% used | memory_used=$memUsed\n";
		exit $exitStatus{critical};
	} elsif ($memUsed > $warning[0]){
		print "Memory WARNING: $memUsed% used | memory_used=$memUsed\n";
		exit $exitStatus{warning};
	} else {
		print "Memory OK: $memUsed% used | memory_used=$memUsed\n";
		exit $exitStatus{ok};
	}
}

# Check CPU
sub checkCpu{
	my @cpu = (0, 0, 0);
		
	# Get values
	# CPU 5 Second value
	my $rCpu5s = $session->get_request(-varbindlist=> [$switch{$device}{cpu}{value5s}]);
	$cpu[0] = $rCpu5s->{$switch{$device}{cpu}{value5s}};
		
	# Catalyst devices supply additional values:
	if($device eq "catalyst"){
		# CPU 1 Minute value
		my $rCpu1m = $session->get_request(-varbindlist=> [$switch{$device}{cpu}{value1m}]);
		$cpu[1] = $rCpu1m->{$switch{$device}{cpu}{value1m}};
		# CPU 5 Minute value
		my $rCpu5m = $session->get_request(-varbindlist=> [$switch{$device}{cpu}{value5m}]);
		$cpu[2] = $rCpu5m->{$switch{$device}{cpu}{value5m}};
				
		# Check Status of all CPU-Values
		my $lWarn = 0;
		my $lCrit = 0;
		for (my $i = 0; $i <= 2; $i++){
			if (!$critical[$i]){
				if (!$critical[0]){
					$critical[$i] = 75;
				} else {
					$critical[$i] = $critical[0];
				}
			}
			if (!$warning[$i]){
				if (!$warning[0]){
					$warning[$i] = 50;
				} else {
					$warning[$i] = $warning[0];
				}
			}
			if($cpu[$i] > $critical[$i]){
				$lCrit++;
			} elsif($cpu[$i] > $warning[$i]) {
				$lWarn++;
			}
		}
	
		# Generate output
		if($lCrit > 0){
			print "CPU CRITICAL: 5s: $cpu[0]% 1m: $cpu[1]% 5m: $cpu[2]% | cpu_5s=$cpu[0] cpu_1m=$cpu[1] cpu_5m=$cpu[2]\n";
			exit $exitStatus{critical};
		} elsif ($lWarn > 0){
			print "CPU WARNING: 5s: $cpu[0]% 1m: $cpu[1]% 5m: $cpu[2]% | cpu_5s=$cpu[0] cpu_1m=$cpu[1] cpu_5m=$cpu[2]\n";
			exit $exitStatus{warning};
		} else {
			print "CPU OK: 5s: $cpu[0]% 1m: $cpu[1]% 5m: $cpu[2]% | cpu_5s=$cpu[0] cpu_1m=$cpu[1] cpu_5m=$cpu[2]\n";
			exit $exitStatus{ok};
		}
	} else {
		# Single-value-devices
		
		if (!$critical[0]){
			$critical[0] = 75;
		}
		if (!$warning[0]){
			$warning[0] = 50;
		}
		
		# Generate output
		if($cpu[0] > $critical[0]){
			print "CPU CRITICAL: $cpu[0]% | cpu=$cpu[0]\n";
			exit $exitStatus{critical};
		} elsif ($cpu[0] > $warning[0]){
			print "CPU WARNING: $cpu[0]% | cpu=$cpu[0]\n";
			exit $exitStatus{warning};
		} else {
			print "CPU OK: $cpu[0]% | cpu=$cpu[0]\n";
			exit $exitStatus{ok};
		}
	}
}

# Check temperature
sub checkTemp{
	my $rTemp = $session->get_table($switch{$device}{temp}{value});
	my $criticals = 0;
	my $warnings = 0;
	my $string;
	my $sum = 0;
	my $perfdata;
	foreach my $oid (keys %$rTemp){
		my $temp = "$$rTemp{$oid}";
		my $id = $oid;
		$id =~ s/$switch{$device}{temp}{value}\.//;
		my $rName = $session->get_request(-varbindlist=> ["$switch{$device}{temp}{name}.$id"]);
		my $name = $rName->{"$switch{$device}{temp}{name}.$id"};
		if (index($name,",")){
			my @tempName = split ",",$name;
			$name = "$tempName[0]$tempName[1]";
		}  
		
		# If no criticals are supplied
		if (!$critical[$sum]){
			# Try to get them from device
			my $rLimit = $session->get_request(-varbindlist=> ["$switch{$device}{temp}{limit}.$id"]);
			$critical[$sum] = $rLimit->{"$switch{$device}{temp}{limit}.$id"};
			
			# fallback to first supplied value
			if(!$critical[$sum]){
				$critical[$sum] = $critical[0];
			}
			
			# last call: fallback to 60°C 
			if(!$critical[$sum]){
				$critical[$sum] = 60;
			}
		}
		
		# If no warnings are supplied generate from criticals
		if (!$warning[$sum]){
			if ($critical[$sum] >= 100){
				$warning[$sum] = $critical[$sum] - 25;
			} else {
				$warning[$sum] = $critical[$sum] - 10;
			}
		}
		
		# Generate output
		if ($temp > $critical[$sum]){
			$criticals++;
			$string = "$string (C)$name: $temp";
		} elsif ($temp > $warning[$sum]){
			$warnings++;
			$string = "$string (W)$name: $temp";
		}
		
		# Generate perfdata (replacing space with _)
		$name =~ s/\ /_/g;
		$perfdata = "$perfdata $name=$temp";
		$sum++;
	}
	
	my $ok = $sum - $criticals - $warnings;
	if ($criticals > 0){
		print "Temperature CRITICAL: $ok of $sum ok, failed:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "Temperature WARNING: $ok of $sum ok, failed:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "Temperature OK: $sum of $sum |$perfdata\n";
		exit $exitStatus{ok};
	}
}

# Check temperature for mds-devices
sub checkTempMds{
	my $rTemp = $session->get_table($switch{$device}{temp}{value});
	my $criticals = 0;
	my $warnings = 0;
	my $string;
	my $sum = 0;
	my $perfdata;
	foreach my $oid (keys %$rTemp){
		my $temp = "$$rTemp{$oid}";
		my $id = $oid;
		$id =~ s/$switch{$device}{temp}{value}\.//;
		
		# Temp sensors only in this oid-range
		if (($id >= 21590) && ($id <= 21592)){
			my $rName = $session->get_request(-varbindlist=> ["$switch{$device}{temp}{name}.$id"]);
			my $name = $rName->{"$switch{$device}{temp}{name}.$id"};
			if (index($name,",")){
				my @tempName = split ",",$name;
				$name = "$tempName[0]$tempName[1]";
			}  
		
			# If no criticals are supplied
			if (!$critical[$sum]){
				# Try to get them from device
				# MDS report both values
				my $rLimit = $session->get_table("$switch{$device}{temp}{limit}.$id");
				my @limit = (0,0);
				my $a = 0;
				foreach my $rLimitRet (keys %$rLimit){
					$limit[$a] = "$$rLimit{$rLimitRet}";
					$a++;
				}
				
				if ($limit[0] > $limit[1]){
					$warning[$sum] = $limit[1];
					$critical[$sum] = $limit[0];
				} elsif ($limit[1] > $limit[0]){
					$warning[$sum] = $limit[0];
					$critical[$sum] = $limit[1];
				} else {
					$warning[$sum] = $limit[0];
					$critical[$sum] = $limit[0];
				}
				
				
				# fallback to first supplied value
				if(!$critical[$sum]){
					$critical[$sum] = $critical[0];
				}
			
				# last call: fallback to 60°C 
				if(!$critical[$sum]){
					$critical[$sum] = 60;
				}
			}
		
			# If no warnings are supplied generate from criticals
			if (!$warning[$sum]){
				if ($critical[$sum] >= 100){
					$warning[$sum] = $critical[$sum] - 25;
				} else {
					$warning[$sum] = $critical[$sum] - 10;
				}
			}
		
			# Generate output
			if ($temp > $critical[$sum]){
				$criticals++;
				$string = "$string (C)$name: $temp";
			} elsif ($temp > $warning[$sum]){
				$warnings++;
				$string = "$string (W)$name: $temp";
			}
		
			# Generate perfdata (replacing space with _)
			$name =~ s/\ /_/g;
			$perfdata = "$perfdata $name=$temp";
			$sum++;
		}
	}
	
	my $ok = $sum - $criticals - $warnings;
	if ($criticals > 0){
		print "Temperature CRITICAL: $ok of $sum ok, failed:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "Temperature WARNING: $ok of $sum ok, failed:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "Temperature OK: $sum of $sum |$perfdata\n";
		exit $exitStatus{ok};
	}
}

# Check fan
sub checkFan{
	my $rFan = $session->get_table($switch{$device}{fan}{status});
	my $criticals = 0;
	my $warnings = 0;
	my $string;
	my $sum = 0;
	foreach my $oid (keys %$rFan){
		my $fanStatus = "$$rFan{$oid}";
		my $id = $oid;
		$id =~ s/$switch{$device}{fan}{status}\.//;
		my $rName = $session->get_request(-varbindlist=> ["$switch{$device}{fan}{name}.$id"]);
		my $name = $rName->{"$switch{$device}{fan}{name}.$id"};
		
		# Generate results
		if ($fanStatus != $switch{$device}{fan}{normal}){
			if (($switch{$device}{fan}{warning} ne "x") && ($fanStatus == $switch{$device}{fan}{warning})){
				$warnings++;
				$string = "$string $name: warning";
			} else {
				$criticals++;
				$string = "$string $name: critical";
			}
		}
		$sum++;
	}
	
	# Generate output
	my $ok = $sum - $warnings - $criticals;
	if ($criticals > 0){
		print "Fan CRITICAL: $ok of $sum ok, failed:$string | OK=$ok\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "Fan WARNING: $ok of $sum ok, failed:$string | OK=$ok\n";
		exit $exitStatus{warning};
	} else {
		print "Fan OK: $ok of $sum | OK=$ok\n";
		exit $exitStatus{ok};
	}
}

# Check power-supply
sub checkPower{
	my $rPower = $session->get_table($switch{$device}{power}{status});
	my $errors = 0;
	my $string;
	my $sum = 0;
	foreach my $oid (keys %$rPower){
		my $powerStatus = "$$rPower{$oid}";
		my $id = $oid;
		$id =~ s/$switch{$device}{power}{status}\.//;
		my $rName = $session->get_request(-varbindlist=> ["$switch{$device}{power}{name}.$id"]);
		my $name = $rName->{"$switch{$device}{power}{name}.$id"};
		if (index($name,",")){
			my @tempName = split ",",$name;
			$name = $tempName[0];
		} 
		chomp $name;

		# Generate results		
		if (index("$oid",$switch{$device}{power}{filter})>0){
			if ($powerStatus != $switch{$device}{power}{normal}){
				$errors++;
				$string = "$string $name: error";
			}
			$sum++;
		}
	}
	
	# Generate output
	my $ok = $sum - $errors;
	if ($errors > 0){
		print "Power CRITICAL: $ok of $sum ok, failed:$string | OK=$ok\n";
		exit $exitStatus{critical};
	} else {
		print "Power OK: $ok of $sum | OK=$ok\n";
		exit $exitStatus{ok};
	}
}

#        Main
#############################
decodeArguments();
$session = snmpSetup($host, $version, $community);

if ($function eq "mem"){
	if("$device" eq "mds"){
		checkMemoryMds();
	} else {
		checkMemory();
	}
} elsif ($function eq "cpu"){
	checkCpu();
} elsif ($function eq "temp"){
	if ("$device" eq "mds"){
		checkTempMds();
	} else {
		checkTemp();
	}
} elsif ($function eq "fan"){
	checkFan();
} elsif ($function eq "power"){
	checkPower();
} else {
	usage();
}