#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor centreonengine                              #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-07-29 (Sysadminday-release)                           #
##############################################################################


#        Use-Imports
#############################

use strict;
use File::Basename;

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my @warning;
my @critical;
my $executable = "/usr/sbin/centenginestats";

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);


#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Centengine-Stats check-script Version 2016-07-29 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check Check-Execution-Latency\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName (-e path/to/executable) -w <value> -c <value>\n";
	print "\n";
	print "Options:\n";
	print "    -e       path to centenginstats-executable\n";
	print "    -w       warning-value\n";
	print "    -c       critical-value\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-e") {
			$executable = shift(@ARGV);
		}
		
		# Decode warning values
		elsif ("$temp" eq "-w") {
			@warning = split ",",shift(@ARGV);
		}
		
		# Decode critical values
		elsif ("$temp" eq "-c") {
			@critical = split ",",shift(@ARGV);
		}
		
		# All else
		else {
			usage();
		}
	}
	
	if (!$warning[0]){
		$warning[0] = 15;
	}
	if (!$critical[0]){
		$critical[0] = 30;
	}
}

# Check latency
sub checkLatency{
	my @rLatency = `$executable`;
	my $string;
	my $perfdata;
	my $criticals;
	my $warnings;
	my $i;
	foreach my $value (@rLatency){
		chomp $value;
		if ((index $value,"Latency") > 0 ){
			# Split name and data
			my ($name,$data) = split ":",$value;
			# Split data values
			my ($min,$max,$avg) = split "/",$data;
			
			# Remove unwanted stuff
			$avg =~ s/\ //g;
			$avg =~ s/sec//g;
			
			# Generate string and perfdata
			$string = "$string $name: $avg sec";
			$name =~ s/\ /_/g;
			$perfdata = "$perfdata $name=$avg";
			
			# Verifiy critical and warning values
			if (!$critical[$i]){
				$critical[$i] = $critical[0];
			}
			if (!$warning[$i]){
				$warning[$i] = $warning[0];
			}
			
			# Count errors
			if ($avg > $critical[$i]){
				$criticals++;
			} elsif ($avg > $warning[$i]){
				$warnings++;
			}
		}
	}
	
	# Generate output
	if ($criticals > 0){
		print "Latency CRITICAL:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings > 0){
		print "Latency WARNING:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "Latency OK:$string |$perfdata\n";
		exit $exitStatus{ok};
	}
}

#        Main
#############################
decodeArguments();
checkLatency();