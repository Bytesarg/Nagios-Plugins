#!/usr/bin/env perl

##############################################################################
#        Perls script to monitor rittal-cmc-devices                          #
#        By bytesarg (c) 2016                                                #
#        Version: 2016-08-08                                                 #
##############################################################################


#        Use-Imports
#############################

use strict;
use Net::SNMP;
use File::Basename;

#        Global OID-Sets
#############################
my %cmc = (
		psm => {
			name   => "1.3.6.1.4.1.2606.7.4.2.2.1.3",
			value  => "1.3.6.1.4.1.2606.7.4.2.2.1.10",
			raw    => "1.3.6.1.4.1.2606.7.4.2.2.1.11",
			factor => "1.3.6.1.4.1.2606.7.4.2.2.1.7",
			unit   => "1.3.6.1.4.1.2606.7.4.2.2.1.5",
		},
);

#        Global variables
#############################
my $path = dirname($0);
my $scriptName = basename($0);
my $host;
my $community = "public";
my $version = "1";
my $session;
my $major = 2;
my $listDevices = 0;
my $type;

#        status-codes
#############################
my %exitStatus =(ok => 0, warning => 1, critical => 2, unknown => 3);


#        SubFunctons
#############################

# Usage-help
sub usage{
	print "Rittal-CMC check-script Version 2016-08-08 (c) by bytesarg\n";
	print "\n";
	print "Use SNMP to check Rittal CMC devices\n";
	print "\n";
	print "Usage:\n";
	print "$path/$scriptName -H <hostaddress> -C <SNMP-community> (-v [1|2c]) -d <device-number> -t [psm|plug|temp](-s)\n";
	print "\n";
	print "Options:\n";
	print "    -H       hostname or address\n";
	print "    -C       SNMP-read-community-name (default: public)\n";
	print "    -v       SNMP-version (default: 1)\n";
	print "    -d       SNMP-Sub-ID of the sub-device (Default: 2, see option -s)\n";
	print "    -t       Device-type (Default: plug)\n";
	print "    -s       Get list of sub-devices\n";
	print "    -h       print this help-screen\n";
	exit $exitStatus{unknown};
}

# Decode arguments and check them for validity
sub decodeArguments{
	while (@ARGV){
		my $temp = shift(@ARGV);
		
		# Decode host
		if ("$temp" eq "-H") {
			$host = shift(@ARGV);
		}
		
		# Decode community
		elsif ("$temp" eq "-C") {
			$community = shift(@ARGV);
		}
		
		# Decode version
		elsif ("$temp" eq "-v") {
			$version = shift(@ARGV);
		}
		
		# Decode device-id
		elsif ("$temp" eq "-d") {
			$major = shift(@ARGV);
		}
		
		# Decode device-type
		elsif ("$temp" eq "-t") {
			$type = shift(@ARGV);
		}
		
		# Decode list-option
		elsif ("$temp" eq "-s") {
			$listDevices = 1;
		}
		
		# All else
		else {
			usage();
		}
	}
	
	# Verifiy arguments
	if (!$host) {
		usage();
	}
	if (("$type" ne "psm") && ("$type" ne "temp")){
		$type = "plug";
	}
}

# SNMP-initialization
sub snmpSetup{
	my ($snmpSession, $snmpError) = Net::SNMP->session(
														-hostname  => $host,
														-version   => $version,
														-community => $community);
	if ($snmpError) {
		print "SNMP encountered an error: $snmpError\n";
		exit $exitStatus{critical};
	}
	
	return $snmpSession;
}

# Check Plug status
sub checkPlug{
	# Details for PSM
	my $psmID = 5; # First PSM module
	my $psmSize = 10;
	my $psmInfoOffset = 1;

	# Details for plugs
	my $plugID; # PlugID is calculated automatically
	my $plugSize = 9;
	my $plugHighOffset = 2;
	my $plugMediumOffset = 3;
	my $plugValueOffset = 1;
	my $plugLowOffset = 4;
	
	# Initialization
	my $numberOfPlugs = 1;
	my $string;
	my $perfdata;
	my $criticals = 0;
	my $warnings = 0;
	my $sum = 0;
	
	# Get Devices
	while ($numberOfPlugs >= 1) {
		
		# fetch number of Plugs from PSM-device
		my $psmValueID = $psmID + $psmInfoOffset;
		my $rNumberOfPlugs = $session->get_request(-varbindlist=>["$cmc{psm}{value}.$major.$psmValueID"]);
		$numberOfPlugs = $rNumberOfPlugs->{"$cmc{psm}{value}.$major.$psmValueID"};
		if ((index $numberOfPlugs,"PSM") >= 0){
			$numberOfPlugs = substr $numberOfPlugs,3,1;
			
			# Get values for each plug
			my $i = 0;
			$plugID = $psmID + $psmSize + 1;
			
			while ($i < $numberOfPlugs){
				# Plug name
				my $rName = $session->get_request(-varbindlist=>["$cmc{psm}{value}.$major.$plugID"]);
				my $name = $rName->{"$cmc{psm}{value}.$major.$plugID"};
				
				# Plug high-value
				my $highOID = $plugID + $plugHighOffset;
				my $rHigh = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$highOID"]);
				my $high = $rHigh-> {"$cmc{psm}{raw}.$major.$highOID"};
				
				# Plug medium-value
				my $mediumOID = $plugID + $plugMediumOffset;
				my $rMedium = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$mediumOID"]);
				my $medium = $rMedium-> {"$cmc{psm}{raw}.$major.$mediumOID"};
				
				# Plug low-value
				my $lowOID = $plugID + $plugLowOffset;
				my $rLow = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$lowOID"]);
				my $low = $rLow-> {"$cmc{psm}{raw}.$major.$lowOID"};
				
				# Plug value, unit and factor
				my $valueOID = $plugID + $plugValueOffset;
				my $rValue = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$valueOID"]);
				my $value = $rValue-> {"$cmc{psm}{raw}.$major.$valueOID"};
				my $rValueUnit = $session->get_request(-varbindlist=>["$cmc{psm}{unit}.$major.$valueOID"]);
				my $valueUnit = $rValueUnit->{"$cmc{psm}{unit}.$major.$valueOID"};
				my $rValueFactor = $session->get_request(-varbindlist=>["$cmc{psm}{factor}.$major.$valueOID"]);
				my $valueFactor = $rValueFactor->{"$cmc{psm}{factor}.$major.$valueOID"};
				if ($valueFactor < 0){
					$valueFactor = $valueFactor * (-1);
				}
				
				my $valueH = $value / $valueFactor;
				
				# Generate ReturnString
				if (($value > $high) || ($value < $low)){
					$criticals++;
					$string = "$string $name: $valueH $valueUnit";
				} elsif ($value > $medium){
					$warnings++;
					$string = "$string $name: $valueH $valueUnit";
				}
				
				# Generate perfdata
				$name =~ s/\ /_/g;
				$name =~ s/,//g;
				$perfdata = "$perfdata $name=$valueH";
				
				# Calculate new plug-ID
				$plugID = $plugID + $plugSize;
				$sum++;
				$i++;
			}
			
		} else {
			$numberOfPlugs = 0;
		}
		
		$psmID = $plugID;
	}
	
	# Generate output
	my $ok = $sum - $warnings - $criticals;
	if($criticals){
		print "Power CRITICAL: $ok of $sum ok; Failed:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings){
		print "Power WARNING: $ok of $sum ok; Failed:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "Power OK: $sum ok |$perfdata\n";
		exit $exitStatus{ok};
	}
}

# Check PSM
sub checkPSM{
	# Base values for PSM-monitoring
	my @ID = (16,25,34,43,52,61,106,115,124,133,142,151);
	my $HighOffset = 2;
	my $MediumOffset = 3;
	my $ValueOffset = 1;
	my $LowOffset = 4;
	
	my $sum = 0;
	my $criticals = 0;
	my $warnings = 0;
	
	my $string;
	my $perfdata;
	
	while ($sum < 12){
		# Phase Name
		my $rName = $session->get_request(-varbindlist=>["$cmc{psm}{value}.$major.$ID[$sum]"]);
		my $name = $rName->{"$cmc{psm}{value}.$major.$ID[$sum]"};
		
		# high-value
		my $highOID = $ID[$sum] + $HighOffset;
		my $rHigh = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$highOID"]);
		my $high = $rHigh-> {"$cmc{psm}{raw}.$major.$highOID"};
				
		# medium-value
		my $mediumOID = $ID[$sum] + $MediumOffset;
		my $rMedium = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$mediumOID"]);
		my $medium = $rMedium-> {"$cmc{psm}{raw}.$major.$mediumOID"};
				
		# low-value
		my $lowOID = $ID[$sum] + $LowOffset;
		my $rLow = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$lowOID"]);
		my $low = $rLow-> {"$cmc{psm}{raw}.$major.$lowOID"};
				
		# value, unit and factor
		my $valueOID = $ID[$sum] + $ValueOffset;
		my $rValue = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.$major.$valueOID"]);
		my $value = $rValue-> {"$cmc{psm}{raw}.$major.$valueOID"};
		my $rValueUnit = $session->get_request(-varbindlist=>["$cmc{psm}{unit}.$major.$valueOID"]);
		my $valueUnit = $rValueUnit->{"$cmc{psm}{unit}.$major.$valueOID"};
		my $rValueFactor = $session->get_request(-varbindlist=>["$cmc{psm}{factor}.$major.$valueOID"]);
		my $valueFactor = $rValueFactor->{"$cmc{psm}{factor}.$major.$valueOID"};
		if ($valueFactor < 0){
			$valueFactor = $valueFactor * (-1);
		}
				
		my $valueH = $value / $valueFactor;
		
		# Generate ReturnString
		if (($value > $high) || ($value < $low)){
			$criticals++;
			$string = "$string $name: $valueH $valueUnit";
		} elsif ($value > $medium){
			$warnings++;
			$string = "$string $name: $valueH $valueUnit";
		}
				
		# Generate perfdata
		$name =~ s/\ /_/g;
		$name =~ s/,//g;
		$perfdata = "$perfdata $name=$valueH";
				
		$sum++;
	}

	# Generate output
	my $ok = $sum - $warnings - $criticals;
	if($criticals){
		print "PSM CRITICAL: $ok of $sum ok; Failed:$string |$perfdata\n";
		exit $exitStatus{critical};
	} elsif ($warnings){
		print "PSM WARNING: $ok of $sum ok; Failed:$string |$perfdata\n";
		exit $exitStatus{warning};
	} else {
		print "PSM OK: $sum ok |$perfdata\n";
		exit $exitStatus{ok};
	}
}

# Check Temperatur
sub checkTemp{
	# Get Value
	my $rValue = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.1.2"]);
	my $value  = $rValue->{"$cmc{psm}{raw}.1.2"};
	my $rValueFactor = $session->get_request(-varbindlist=>["$cmc{psm}{factor}.1.2"]);
	my $valueFactor  = $rValueFactor->{"$cmc{psm}{factor}.1.2"};
	
	# Find first limit-value:
	my $rBegin = $session->get_request(-varbindlist=>["$cmc{psm}{name}.1.3"]);
	my $begin;
	if ((index $rBegin->{"$cmc{psm}{name}.1.3"},"HighAlarm") >= 0){
		$begin = 3;
	} else {
		$begin = 4;
	}
	
	# Get Limits
	my $rCHigh = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.1.$begin"]);
	my $cHigh  = $rCHigh->{"$cmc{psm}{raw}.1.$begin"};
	$begin++;
	my $rWHigh = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.1.$begin"]);
	my $wHigh  = $rWHigh->{"$cmc{psm}{raw}.1.$begin"};
	$begin++;
	my $rWLow  = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.1.$begin"]);
	my $wLow   = $rWLow->{"$cmc{psm}{raw}.1.$begin"};
	$begin++;
	my $rCLow  = $session->get_request(-varbindlist=>["$cmc{psm}{raw}.1.$begin"]);
	my $cLow   = $rCLow->{"$cmc{psm}{raw}.1.$begin"};
	
	if ($valueFactor < 0){
		$valueFactor = $valueFactor * (-1);
	}
	my $valueH = $value / $valueFactor;
	
	if (($value > $cHigh) || ($value < $cLow)){
		print "Temperatur CRITICAL: $valueH C | C=$valueH\n";
		exit $exitStatus{critical};
	} elsif (($value > $wHigh) || ($value < $wLow)){
		print "Temperatur WARNING: $valueH C | C=$valueH\n";
		exit $exitStatus{warning};
	} else {
		print "Temperatur OK: $valueH C | C=$valueH\n";
		exit $exitStatus{ok};
	}
}

# List devices
sub listDevices{
	$major = 1;
	my $string = "Found devices:\n";
	
	# Get Devices (CMC supports a total of 32 devices)
	while ($major <= 50){
		my $rName = $session->get_request(-varbindlist=>["$cmc{psm}{value}.$major.1"]);
		my $name = $rName->{"$cmc{psm}{value}.$major.1"};
		if ("$name" ne "noSuchInstance"){
			$string = $string."Device $major: $name\n";
		}
		$major++;
	}
	
	# Generate Output
	print $string;
	exit $exitStatus{unknown};
}

#        Main
#############################
decodeArguments();
$session = snmpSetup($host, $version, $community);

if ($listDevices){
	listDevices();
} elsif ("$type" eq "plug") {
	checkPlug();
} elsif ("$type" eq "psm") {
	checkPSM();
} elsif ("$type" eq "temp") {
	checkTemp();
} else {
	usage();
}